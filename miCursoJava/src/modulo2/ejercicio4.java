package modulo2;

public class ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte  b=10;
		short s=20;
		int i = 30;
		long l= 40;
		//Cual de las siguientes líneas dan errores de compilación y para esos casos cubrirlos con el casteo correspondiente
		l = s;
		b=(byte)s; //ERROR
		System.out.println(b);
		l=i;
		b=(byte)i; //ERROR
		s=(short)i; //ERROR
		
		System.out.println(b);
		System.out.println(s);
		
	
	}

}
