package modulo3;
import java.util.Scanner;
public class ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
 realizar un sistema que permita mostrar las caracter�sticas de un auto 
 utilizando para ello una variable de tipo char, esta se llenara con los
  valores �a�, �b� o �c�, siendo de clase �a� los que 
  tienen 4 ruedas y un motor, clase �b� 4 ruedas,  un motor, cierre centralizado 
  y aire, y clase �c� 4 ruedas,  un motor, cierre centralizado, aire, airbag.
 */
		System.out.println("Seleccione una clase: A - B - C - D");
		Scanner v = new Scanner(System.in);
		char valor;
		valor=v.next().charAt(0);
		System.out.println(valor);
		switch(valor){
		case 'A':
			System.out.println("tienen 4 ruedas y un motor");
			break;
		case 'B':
			System.out.println("tienen 4 ruedas,  un motor, cierre centralizado y aire");
			break;
		case 'C':
			System.out.println("tienen 4 ruedas,  un motor, cierre centralizado, aire, airbag");
			break;
		default: 
			System.out.println("No seleccionaste ninguna clase");
		}
	}

}
